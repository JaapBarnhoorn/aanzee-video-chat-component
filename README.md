# Aan Zee video chat component

A simple WebRTC video chat component built with Svelte and PeerJS.

## Install

`npm i @aanzee/aanzee-video-chat-component`

# Usage

The package includes exports for raw svelte, ES Module(.mjs)) and CJS (.js) exports. Your bundler will likely know which one to pick by using import VideoComponent from 'aanzee-video-chat-component'.

```html
<script>
  import VideoComponent from '@aanzee/aanzee-video-chat-component';
  const userId = '...'
	let showDeviceSettings = true|false boolean;
	const server = {
		host: 'localhost',
		port: 9000
	}
</script>

<VideoComponent userId={userId} showDeviceSettings="{showDeviceSettings}" server="{server}" />
```

## Events
Dispatch a `'call'` event with a userId you want to call to initiate a call. e.g.:
```js
// Dispatch the makeCallEvent to initiate a call with userId
function makeCall(userId) {
	connectedUser = userId;
	const makeCallEvent = new CustomEvent('call', {
		detail: {
			userId
		}
	});
	window.dispatchEvent(makeCallEvent);
}
```

Dispatch an `'endCall'` event to end the current call. e.g.:
```js
// create endCall event
const endCallEvent = new Event('endCall');

// Dispatch the endCallEvent
function endCall() {
	window.dispatchEvent(endCallEvent);
}
```

You can attach the events to a button click for example.This generic setup enables you to place the buttons where you want.

## Parcel error
If you get an error stating that parcelRequire cannot be defined, add the following script before loading the component (for example in `<head>`):
```html
<script>
	window.global = window;
	var parcelRequire;
</script>
```

## Props
| Prop                 | Type      | Description                                                                                |
| -------------------- |-----------| -------------------------------------------------------------------------------------------|
| `userId`             | string    | Pass the userId of the currently logged in user. It will be used as the peer connection ID |
| `showDeviceSettings` | boolean   | Handle showing of the device settings                                                      |
| `server`             | object    | Enter the peer server details (default: { host: 'localhost, port: 9000 })                  |


# Development

## Installation
Clone the project from BitBucket and install the dependencies:

```bash
npm install
```

## Starting the peer server

Start local PeerJs server to enable peer conections:

First install [Peer Server](https://github.com/peers/peerjs-server)

`npm install peer -g`

It will run on port `:9000` by default

Then run the local server:

```bash
npm run serve
```
## Start developing

To start developing run:

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and the page should reload and you will see yout changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.

## Building

```bash
npm run build
```
