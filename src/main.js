import VideoComponent from './VideoComponent.svelte';

const videoComponent = new VideoComponent({
  target: document.body,
  props: {
    userId: Math.floor(Math.random() * 1e7).toString(),
    showDeviceSettings: false,
  },
});

export default videoComponent;
